CHELSA_EarthEnv
-----------
This package contains functions to creates dauky high-resolution 
cloud cover refined precipiation using CHELSA V2.1 as 
baseline daily precipiation input. It is part of the
CHELSA Project: (CHELSA, <https://www.chelsa-climate.org/>).




COPYRIGHT
---------
(C) 2021 Dirk Nikolaus Karger



LICENSE
-------
CHELSA_EarthEnv is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

CHELSA_EarthEnv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with CHELSA_EarthEnv. If not, see <http://www.gnu.org/licenses/>.



REQUIREMENTS
------------
CHELSA_EarthEnv is written in Python 2.7. It has been tested to run well with the
following Python release and package versions.
- python 2.7
- saga_api 7.9.1
- sys 2.7.15+
- argparse 1.1


HOW TO USE
----------
The chelsa_cmip6 module provides functions to creates daily cloud cover refined
precipiation data based on chelsa v2.1 daily data.

The saga_functions module contains functions that are neccessary for the
downscaling and the cloud cover refinement. 


CITATION:
------------
If you need a citation for the output, please refer to the arcticle describing the high
resolution data:

DN Karger, AM Wilson, C Mahony, NE Zimmermann, W Jetz 'Global daily 1km land surface precipitation based on cloud cover-informed downscaling', Scientific Data 8, 307 (2021)


CONTACT
-------
<dirk.karger@wsl.ch>



AUTHOR
------
Dirk Nikolaus Karger
Swiss Federal Research Institute WSL
Zürcherstrasse 111
8903 Birmensdorf 
Switzerland

