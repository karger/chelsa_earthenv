#!/usr/bin/env python

#This file is part of CHELSA_EarthEnv.
#
#CHELSA_EarthEnv is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#CHELSA_EarthEnv is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with CHELSA_EarthEnv.  If not, see <https://www.gnu.org/licenses/>.

import saga_api


def Load_Tool_Libraries(Verbose):
    saga_api.SG_UI_Msg_Lock(True)
    if os.name == 'nt':    # Windows
        os.environ['PATH'] = os.environ['PATH'] + ';' + os.environ['SAGA_32'] + '/dll'
        saga_api.SG_Get_Tool_Library_Manager().Add_Directory(os.environ['SAGA_32' ] + '/tools', False)
    else:                  # Linux
        saga_api.SG_Get_Tool_Library_Manager().Add_Directory('/usr/local/lib/saga/', False)        # Or set the Tool directory like this!
    saga_api.SG_UI_Msg_Lock(False)

    if Verbose == True:
                print 'Python - Version ' + sys.version
                print saga_api.SAGA_API_Get_Version()
                print 'number of loaded libraries: ' + str(saga_api.SG_Get_Tool_Library_Manager().Get_Count())
                print

    return saga_api.SG_Get_Tool_Library_Manager().Get_Count()


def load_sagadata(path_to_sagadata):

    saga_api.SG_Set_History_Depth(0)    # History will not be created
    saga_api_dataobject = 0             # initial value

    # CSG_Grid -> Grid
    if any(s in path_to_sagadata for s in (".sgrd", ".sg-grd", "sg-grd-z")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Grid(unicode(path_to_sagadata))

    # CSG_Grids -> Grid Collection
    if any(s in path_to_sagadata for s in ("sg-gds", "sg-gds-z")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Grids(unicode(path_to_sagadata))

    # CSG_Table -> Table
    if any(s in path_to_sagadata for s in (".txt", ".csv", ".dbf")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Table(unicode(path_to_sagadata))

    # CSG_Shapes -> Shapefile
    if '.shp' in path_to_sagadata:
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Shapes(unicode(path_to_sagadata))

    # CSG_PointCloud -> Point Cloud
    if any(s in path_to_sagadata for s in (".spc", ".sg-pts", ".sg-pts-z")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_PointCloud(unicode(path_to_sagadata))

    if saga_api_dataobject == None or saga_api_dataobject.is_Valid() == 0:
        print 'ERROR: loading [' + path_to_sagadata + ']'
        return 0

    print 'File: [' + path_to_sagadata + '] has been loaded'
    return saga_api_dataobject


def import_gdal(File):
    #_____________________________________
    # Create a new instance of tool 'Import Raster'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('io_gdal', '0')
    if Tool == None:
        print 'Failed to create tool: Import Raster'
        return False

    Parm = Tool.Get_Parameters()
    Parm('FILES').Set_Value(File)
    Parm('MULTIPLE').Set_Value('automatic')
    Parm('TRANSFORM').Set_Value(False)
    Parm('RESAMPLING').Set_Value('Nearest Neighbour')

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay'

    #_____________________________________
    output = Tool.Get_Parameter(saga_api.CSG_String('GRIDS')).asGridList().Get_Grid(0)
    # _____________________________________

    return output


def multiply_prec_cc(input1,input2):

    #_____________________________________
    # Create a new instance of tool 'Grid Calculator'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('grid_calculus', '1')
    if Tool == None:
        print 'Failed to create tool: Grid Calculator'
        return False

    Parm = Tool.Get_Parameters()
    Parm.Reset_Grid_System()
    Parm('RESAMPLING').Set_Value('B-Spline Interpolation')
    Parm('FORMULA').Set_Value('a*(b/10000)')
    Parm('NAME').Set_Value('expected')
    Parm('FNAME').Set_Value(False)
    Parm('USE_NODATA').Set_Value(False)
    Parm('TYPE').Set_Value('4 byte floating point number')
    Parm('GRIDS').asList().Add_Item(input1)
    Parm('XGRIDS').asList().Add_Item(input2)

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay'
    # Parm('RESULT').asDataObject().Save(Path + Parm('RESULT').asDataObject().Get_Name() + '.sgrd')
    biascor_grid = Parm('RESULT').asGrid()
    # remove this tool instance, if you don't need it anymore
    saga_api.SG_Get_Tool_Library_Manager().Delete_Tool(Tool)

    return biascor_grid


def resample_to_coarse(Memobj):
    #_____________________________________
    # Create a new instance of tool 'Resampling'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('grid_tools', '0')
    if Tool == None:
        print('Failed to create tool: Resampling')
        return False

    Parm = Tool.Get_Parameters()
    Parm.Reset_Grid_System()
    Parm('INPUT').asList().Add_Item(Memobj)
    Parm('KEEP_TYPE').Set_Value(False)
    Parm('SCALE_UP').Set_Value('Mean Value (cell area weighted)')
    Parm('TARGET_DEFINITION').Set_Value('user defined')
    Parm('TARGET_USER_SIZE').Set_Value(0.250000)
    Parm('TARGET_USER_XMIN').Set_Value(-180.000000)
    Parm('TARGET_USER_XMAX').Set_Value(180.000000)
    Parm('TARGET_USER_YMIN').Set_Value(-90.000000)
    Parm('TARGET_USER_YMAX').Set_Value(90.000000)
    Parm('TARGET_USER_COLS').Set_Value(1441)
    Parm('TARGET_USER_ROWS').Set_Value(721)
    Parm('TARGET_USER_FITS').Set_Value('nodes')

    print('Executing tool: ' + Tool.Get_Name().c_str())
    if Tool.Execute() == False:
        print('failed')
        return False
    print('okay')

    output = Tool.Get_Parameter(saga_api.CSG_String('OUTPUT')).asGridList().Get_Grid(0)
    #_____________________________________

    return output


def downscale_prec(Agrid,Bgrid,Cgrid):

    #_____________________________________
    # Create a new instance of tool 'Grid Calculator'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('grid_calculus', '1')
    if Tool == None:
        print 'Failed to create tool: Grid Calculator'
        return False

    Parm = Tool.Get_Parameters()
    Parm.Reset_Grid_System()
    Parm('RESAMPLING').Set_Value('B-Spline Interpolation')
    Parm('FORMULA').Set_Value('b*a/c')
    Parm('NAME').Set_Value('downscaled')
    Parm('FNAME').Set_Value(False)
    Parm('USE_NODATA').Set_Value(False)
    Parm('TYPE').Set_Value('unsigned 2 byte integer')
    Parm('GRIDS').asList().Add_Item(Agrid)
    Parm('XGRIDS').asList().Add_Item(Bgrid)
    Parm('XGRIDS').asList().Add_Item(Cgrid)

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay'
    # Parm('RESULT').asDataObject().Save(Path + Parm('RESULT').asDataObject().Get_Name() + '.sgrd')
    downscaled = Parm('RESULT').asDataObject()

    return downscaled


def export_geotiff(OBJ,outputfile):
    #_____________________________________
    # Create a new instance of tool 'Export GeoTIFF'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('io_gdal', '2')
    if Tool == None:
        print 'Failed to create tool: Export GeoTIFF'
        return False

    Parm = Tool.Get_Parameters()
    Parm.Reset_Grid_System()
    Parm('GRIDS').asList().Add_Item(OBJ)
    Parm('FILE').Set_Value(outputfile)
    Parm('OPTIONS').Set_Value('COMPRESS=DEFLATE PREDICTOR=2')

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay - geotiff created'

    #_____________________________________
    # remove this tool instance, if you don't need it anymore
    saga_api.SG_Get_Tool_Library_Manager().Delete_Tool(Tool)

    return True


def build_cc(File1,File2):
    #_____________________________________
    # Create a new instance of tool 'Mosaicking'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('grid_tools', '3')
    if Tool == None:
        print 'Failed to create tool: Mosaicking'
        return False

    Parm = Tool.Get_Parameters()
    Parm('GRIDS').asList().Add_Item(File1)
    Parm('GRIDS').asList().Add_Item(File2)
    Parm('NAME').Set_Value('Mosaic')
    Parm('TYPE').Set_Value('4 byte floating point')
    Parm('RESAMPLING').Set_Value('B-Spline Interpolation')
    Parm('OVERLAP').Set_Value('last')
    Parm('BLEND_DIST').Set_Value(10.000000)
    Parm('BLEND_BND').Set_Value('valid data cells')
    Parm('MATCH').Set_Value('none')
    Parm('TARGET_DEFINITION').Set_Value('user defined')
    Parm('TARGET_USER_SIZE').Set_Value(926.6254330558)
    Parm('TARGET_USER_XMIN').Set_Value(-20016035.97943905)
    Parm('TARGET_USER_XMAX').Set_Value(20015109.35400457)
    Parm('TARGET_USER_YMIN').Set_Value(-10002921.549837718)
    Parm('TARGET_USER_YMAX').Set_Value(10001994.92440395)
    Parm('TARGET_USER_COLS').Set_Value(43202)
    Parm('TARGET_USER_ROWS').Set_Value(21590)
    Parm('TARGET_USER_FITS').Set_Value('nodes')

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay - cloud cover put together'

    #_____________________________________

    cc = Parm('TARGET_OUT_GRID').asGrid()

    #_____________________________________

    return cc


def reproj_grid(File):
    #_____________________________________
    # Create a new instance of tool 'Coordinate Transformation (Grid)'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('pj_proj4', '3')
    if Tool == None:
        print('Failed to create tool: Coordinate Transformation (Grid)')
        return False
    Parm = Tool.Get_Parameters()
    Parm('CRS_METHOD').Set_Value('Proj4 Parameters')
    Parm('CRS_PROJ4').Set_Value('+proj=longlat +datum=WGS84 +no_defs ')
    #Parm('CRS_FILE').Set_Value('')
    Parm('CRS_EPSG').Set_Value(4326)
    Parm('CRS_EPSG_AUTH').Set_Value('EPSG')
    Parm('PRECISE').Set_Value(False)
    Parm('SOURCE').asList().Add_Item(File)
    Parm('RESAMPLING').Set_Value('B-Spline Interpolation')
    Parm('KEEP_TYPE').Set_Value(True)
    Parm('TARGET_AREA').Set_Value(False)
    Parm('TARGET_DEFINITION').Set_Value('user defined')
    Parm('TARGET_USER_SIZE').Set_Value(0.0083333333)
    Parm('TARGET_USER_XMIN').Set_Value(-179.9959722222)
    Parm('TARGET_USER_XMAX').Set_Value(179.9956930045)
    Parm('TARGET_USER_YMIN').Set_Value(-89.9959722222)
    Parm('TARGET_USER_YMAX').Set_Value(83.9956937485)
    Parm('TARGET_USER_COLS').Set_Value(43200)
    Parm('TARGET_USER_ROWS').Set_Value(20880)
    Parm('TARGET_USER_FITS').Set_Value('nodes')

    print('Executing tool: ' + Tool.Get_Name().c_str())
    if Tool.Execute() == False:
        print('failed')
        return False
    print('okay')

    respamp = Tool.Get_Parameter(saga_api.CSG_String('GRIDS')).asGridList().Get_Grid(0)

    return respamp


def calc_mean(Agrid,Bgrid):

    #_____________________________________
    # Create a new instance of tool 'Grid Calculator'
    Tool = saga_api.SG_Get_Tool_Library_Manager().Create_Tool('grid_calculus', '1')
    if Tool == None:
        print 'Failed to create tool: Grid Calculator'
        return False

    Parm = Tool.Get_Parameters()
    Parm.Reset_Grid_System()
    Parm('RESAMPLING').Set_Value('B-Spline Interpolation')
    Parm('FORMULA').Set_Value('(b+a)/2')
    Parm('NAME').Set_Value('downscaled')
    Parm('FNAME').Set_Value(False)
    Parm('USE_NODATA').Set_Value(False)
    Parm('TYPE').Set_Value('unsigned 2 byte integer')
    Parm('GRIDS').asList().Add_Item(Agrid)
    Parm('XGRIDS').asList().Add_Item(Bgrid)

    print 'Executing tool: ' + Tool.Get_Name().c_str()
    if Tool.Execute() == False:
        print 'failed'
        return False
    print 'okay'
    meangrid = Parm('RESULT').asDataObject()

    return meangrid

