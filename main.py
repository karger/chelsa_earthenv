#!/usr/bin/env python

#This file is part of CHELSA_EarthEnv.
#
#CHELSA_EarthEnv is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#CHELSA_EarthEnv is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with CHELSA_EarthEnv.  If not, see <https://www.gnu.org/licenses/>.

import saga_api
import sys
import os
import argparse
import datetime
import os.path
from functions/saga_functions.py import *

# *************************************************
# Get the command line arguments
# *************************************************
ap = argparse.ArgumentParser()

ap.add_argument('-a','--time', type=int, help="time iteration starting at the specified time")
ap.add_argument('-b','--startyear', type=int, help="start year")
ap.add_argument('-c','--startmonth', type=int, help="start month")
ap.add_argument('-d','--startday', type=int,  help="start day")
ap.add_argument('-e','--endyear', type=int,  help="end year")
ap.add_argument('-f','--endmonth', type=int,  help="end month")
ap.add_argument('-g','--endday', type=int,  help="end day")
ap.add_argument('-i','--input', type=str, help="input directory pointing to the CHELSA V2 precipitation data")
ap.add_argument('-o','--output', type=str,  help="output directory")
ap.add_argument('-cc','--cloudcover', type=str, help="input directory pointing to the MODIS cloud cover data")
ap.add_argument('-cp','--calculateprec', type=int, help="int should precipitation be calculated or only monthly cloud cover")
args = ap.parse_args()

# *************************************************
# Create a vector of times
print(args)
print(args.time)

start_day= args.startday
print(start_day)
dt = datetime.datetime(args.startyear,
                       args.startmonth,
                       args.startday)
print(dt)

end = datetime.datetime(args.endyear,
                        args.endmonth,
                        args.endday)
step = datetime.timedelta(seconds=86400)

result = []

while dt < end:
    result.append(dt)
    dt += step

print(result[1])
print(result[2])

date = result[args.time]

year = "%02d" % date.year
day = "%02d" % date.day
month = "%02d" % date.month
month1 = "%01d" % date.month


# *************************************************
# Set the input files and load them into the datastore
# *************************************************

INPUT = args.input
OUTPUT = args.output
MODIS = args.cloudcover

calculate_prec = args.calculateprec

file1 = INPUT + 'CHELSA_preccor_' + day + '_' + month + '_' + year + '_V2.0.tif'
outputfile = OUTPUT + 'CHELSA_preccor_land_' + day + '_' + month + '_' + year + '_V.2.1.tif'
cloud_out  = MODIS + 'MODIS_CC' + month + '_' + year + '.tif'

ccfile1 = MODIS + '2017117_MonthlyCF_MOD09GA_' + year + '_' + month1 + '0000000000-0000000000.tif'
ccfile2 = MODIS + '2017117_MonthlyCF_MOD09GA_' + year + '_' + month1 + '0000000000-0000023296.tif'

ccfile3 = MODIS + '2017117_MonthlyCF_MYD09GA_' + year + '_' + month1 + '0000000000-0000000000.tif'
ccfile4 = MODIS + '2017117_MonthlyCF_MYD09GA_' + year + '_' + month1 + '0000000000-0000023296.tif'

if __name__ == '__main__':

    Load_Tool_Libraries(True)

    if (os.path.isfile(outputfile) == True):

        print 'CHELSA MODCF starting calulation...'
        if calculate_prec is 0:
            # get the input datasets
            if (date.day != 1):
                sys.exit('creation only needed ones per month')

            prec_org = import_gdal(file1)

            if (os.path.isfile(ccfile1) == False):
                ccfile1 = ccfile3
                ccfile2 = ccfile4

            if (os.path.isfile(ccfile1) == True):
                cc1 = import_gdal(ccfile1)
                cc2 = import_gdal(ccfile2)
                cc3 = import_gdal(ccfile3)
                cc4 = import_gdal(ccfile4)

            # combine the two cloud files two one
            cloudccf = build_cc(cc1,cc2)

            cloudccf2 = build_cc(cc2,cc3)

            # free up memory
            saga_api.SG_Get_Data_Manager().Delete(cc1)
            saga_api.SG_Get_Data_Manager().Delete(cc2)
            saga_api.SG_Get_Data_Manager().Delete(cc3)
            saga_api.SG_Get_Data_Manager().Delete(cc4)

            # reproject to geographic coordinate system
            cloudcf_mean = calc_mean(cloudccf, 
                                     cloudccf2)

            cloudccf2 = reproj_grid(cloudcf_mean)

            print('saving cloud file')
            export_geotiff(cloudccf2,cloud_out)
            print(cloud_out + 'saved')


        if calculate_prec is 1:
            print ('start prec calc')
            
            prec_org = import_gdal(file1)

            cloudccf2 = import_gdal(cloud_out)

            # Create a bias corrected prec expectation
            bias_cor_grid = multiply_prec_cc(prec_org,
                                             cloudccf2)

            # resample to coarse resolution
            bias_cor_resampled = resample_to_coarse(bias_cor_grid)
            prec_resampled = resample_to_coarse(prec_org)

            # free up memory
            saga_api.SG_Get_Data_Manager().Delete(prec_org)

            # downscale to resolution
            prec_high = downscale_prec(bias_cor_grid,
                                       prec_resampled,
                                       bias_cor_resampled)

            # export to geotiff
            export_geotiff(prec_high,
                           outputfile)

            print saga_api.SG_Get_Data_Manager().Get_Summary().c_str()
            # job is done, free memory resources
            saga_api.SG_Get_Data_Manager().Delete_All()

            print 'CHELSA calculation finished'
